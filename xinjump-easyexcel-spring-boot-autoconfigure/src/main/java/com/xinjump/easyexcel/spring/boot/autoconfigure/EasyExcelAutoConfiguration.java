package com.xinjump.easyexcel.spring.boot.autoconfigure;

import com.xinjump.easyexcel.event.ExcelManager;
import com.xinjump.easyexcel.spring.boot.properties.EasyExcelProperties;
import com.xinjump.easyexcel.spring.boot.properties.EasyExcelSimpleProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/25 17:11
 * @see com.xinjump.easyexcel.spring.boot.autoconfigure
 */
@Configuration
@EnableConfigurationProperties({EasyExcelSimpleProperties.class})
@ConditionalOnProperty(prefix = EasyExcelProperties.EASY_EXCEL_PREFIX, value = "packPath", matchIfMissing = true)
//@Import({ExcelHandlerImpl.class})
public class EasyExcelAutoConfiguration {

    @Bean
    public ExcelManager excelManager(EasyExcelSimpleProperties easyExcelSimpleProperties,
                                     ApplicationContext applicationContext) throws Exception {
        ExcelManager excelManager = new ExcelManager(easyExcelSimpleProperties.getPackPath());
        excelManager.setApplicationContext(applicationContext);
        excelManager.afterPropertiesSet();
        return excelManager;
    }

}
