package com.xinjump.easyexcel.spring.boot.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/1/19 12:27
 * @see com.xinjump.easyexcel.spring.boot.properties
 */
@ConfigurationProperties(prefix = EasyExcelProperties.EASY_EXCEL_PREFIX)
public class EasyExcelSimpleProperties {

    private String packPath;

    public String getPackPath() {
        return packPath;
    }

    public void setPackPath(String packPath) {
        this.packPath = packPath;
    }

}
