package com.xinjump.easyexcel.spring.boot.properties;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/1/19 12:26
 * @see com.xinjump.easyexcel.spring.boot.properties
 */
public class EasyExcelProperties {

    public static final String EASY_EXCEL_PREFIX = "easyexcel";

}
