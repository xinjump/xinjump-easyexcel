# xinjump-easyexcel

### 发布
1.0.1 初版基于easyexcel自动化导入导出，根据type指定侦听事件

1.0.2 支持导入有错误数据时函数消费

1.0.3 支持查询进度条

### 介绍
alibaba easyexcel demo.
1. 基于注解@ExcelListener导入、导出、下载Excel；支持批导方式（BatchModel对象）
2. 提供AnalysisEventAutoListener抽象类实现数据检验、存储数据方法
3. 获取IOC容器中ExcelHandler接口，调用通用导入导出
4. spring boot 开箱即用

### 软件架构
基于alibaba easyexcel封装个人工具

### 使用说明
只支持spring boot 方式 maven 依赖
```xml
  <dependency>
      <groupId>com.gitee.xinjump</groupId>
      <artifactId>xinjump-easyexcel-spring-boot-starter</artifactId>
      <version>1.1.1</version>
  </dependency>
```
配置文件配置，方便初始化读取您的监听实例，否则会报错:"请指定excel监听实例路径"，多个地址使用英文逗号截取
````yaml
easyexcel.pack-path=com.xinjump.easyexcel.example.excel,com.xinjump.easyexcel.example2.excel
````
