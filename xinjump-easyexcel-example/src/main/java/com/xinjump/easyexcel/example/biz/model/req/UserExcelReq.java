package com.xinjump.easyexcel.example.biz.model.req;

import com.alibaba.excel.annotation.ExcelProperty;
import com.xinjump.easyexcel.annotation.ExcelSelected;
import com.xinjump.easyexcel.example.biz.service.impl.UserExcelSelectImpl;
import com.xinjump.easyexcel.example.enums.BooleanEnum;
import com.xinjump.easyexcel.ExcelRow;
import com.xinjump.easyexcel.annotation.Excel;
import com.xinjump.easyexcel.annotation.ExcelValidated;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/11/20 14:59
 */
@Excel(fileName = "123456789fileName", sheetName = "我是sheet")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
public class UserExcelReq extends ExcelRow {

    @NotNull(message = "用户ID不可为空")
    @ExcelProperty(value = "用户ID一", index = 0)
    private Long userId;

    @ExcelSelected(/*source = {"张三", "李四"}, */sourceClass = UserExcelSelectImpl.class)
    @NotBlank(message = "用户名称不可为空")
    @ExcelProperty(value = "用户名称", index = 1)
    private String userName;

    @ExcelValidated(enumClass = BooleanEnum.class, message = "账户不合法")
    @ExcelProperty(value = "账户", index = 2)
    private String account;

    @Length(max = 10, message = "密码值超长")
    @ExcelProperty(value = "密码", index = 3)
    private String password;

}
