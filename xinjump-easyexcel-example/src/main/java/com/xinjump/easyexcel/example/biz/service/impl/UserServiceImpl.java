package com.xinjump.easyexcel.example.biz.service.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.xinjump.easyexcel.example.biz.entity.UserEntity;
import com.xinjump.easyexcel.example.biz.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/16 14:55
 * @see com.xinjump.easyexcel.example.biz.service.impl
 */
@Service
public class UserServiceImpl implements UserService {

    @Override
    public void query() {
        System.err.println("123456 - UserServiceImpl");
    }

    @Override
    public boolean saveOrUpdateBatch(List<UserEntity> userEntityList) {
        System.err.println("userEntityList: " + JSON.toJSONString(userEntityList));
        return true;
    }

    @Override
    public List<UserEntity> selectAll() {
        UserEntity userEntity1 = new UserEntity();
        UserEntity userEntity2 = new UserEntity();
        userEntity1.setUserName("张三");
        userEntity2.setUserName("李四");
        return Lists.newArrayList(userEntity1, userEntity2);
    }

}
