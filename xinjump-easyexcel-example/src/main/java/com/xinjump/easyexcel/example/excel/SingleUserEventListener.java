package com.xinjump.easyexcel.example.excel;

import com.xinjump.easyexcel.ExcelResult;
import com.xinjump.easyexcel.event.AnalysisEventAutoListener;
import com.xinjump.easyexcel.example.biz.model.req.UserExcelReq;
import com.xinjump.easyexcel.example.biz.service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class SingleUserEventListener extends AnalysisEventAutoListener<UserExcelReq> {

    private String creator;
    private UserService userService;

    public SingleUserEventListener(String creator,
                                   UserService userService) {
        super(1000, Arrays.asList("用户ID一", "用户名称", "账户", "密码"));
        this.creator = creator;
        this.userService = userService;
    }

    @Override
    protected ExcelResult<UserExcelReq> checkData(List<UserExcelReq> rows) {
        return null;
    }

    @Override
    protected Function<List<UserExcelReq>, Object> storageDataFun() {
        return null;
    }

}
