package com.xinjump.easyexcel.example.biz.model.res;

import com.alibaba.excel.annotation.ExcelProperty;
import com.xinjump.easyexcel.annotation.Excel;
import com.xinjump.easyexcel.annotation.ExcelSelected;
import com.xinjump.easyexcel.example.biz.service.impl.UserExcelSelectImpl;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/11/20 14:59
 */
@Excel(sheetName = "sheet1", fileName = "用户信息")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserExcelRes {

    @ExcelProperty(value = "用户ID", index = 0)
    private Long userId;

    @ExcelSelected(/*source = {"张三", "李四"}, */sourceClass = UserExcelSelectImpl.class)
    @ExcelProperty(value = "用户名称", index = 1)
    private String userName;

    @ExcelProperty(value = "账户", index = 2)
    private String account;

    @ExcelProperty(value = "密码", index = 3)
    private String password;

}
