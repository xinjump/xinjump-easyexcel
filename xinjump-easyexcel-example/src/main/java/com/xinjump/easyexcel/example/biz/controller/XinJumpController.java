package com.xinjump.easyexcel.example.biz.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.BooleanUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.google.common.collect.Lists;
import com.xinjump.easyexcel.batch.BatchFetchExecuteAdaptor;
import com.xinjump.easyexcel.cache.Cache;
import com.xinjump.easyexcel.cache.CacheManager;
import com.xinjump.easyexcel.example.biz.model.req.UserExcelReq;
import com.xinjump.easyexcel.example.biz.model.res.UserExcelRes;
import com.xinjump.easyexcel.example.biz.service.UserService;
import com.xinjump.easyexcel.example.converter.UserConverter;
import com.xinjump.easyexcel.example.excel.SingleUserEventListener;
import com.xinjump.easyexcel.example.excel.UserEventListener;
import com.xinjump.easyexcel.helper.EasyExcelHelper;
import com.xinjump.easyexcel.util.FastJsonUtil;
import com.xinjump.easyexcel.write.model.WriteSheetModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/9/2 15:19
 */
@RestController
public class XinJumpController {

    @Autowired
    private UserService userService;

    @GetMapping("/downloadTemplate")
    public void downloadTemplate(HttpServletResponse response) throws Exception {

        List<UserExcelReq> list = Lists.newArrayList();
        list.add(UserExcelReq.builder()
                .userId(111L)
                .userName("张三")
                .password("1234512345672345678345564864456456444455414566789")
                .build());
        EasyExcelHelper.downloadTemplate(response, UserExcelReq.class, list);
    }

    /**
     * 单sheet导入
     */
    @GetMapping("/singleimport")
    public ResponseEntity read(@RequestPart MultipartFile file) throws Exception {

        EasyExcelHelper.read(file.getInputStream(), new SingleUserEventListener("system", userService));
        return ResponseEntity.ok().build();
    }

    /**
     * 多sheet导入
     */
    @GetMapping("/import")
    public ResponseEntity read(@RequestParam String types, @RequestPart MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String[] type = types.split(",");
        boolean read = EasyExcelHelper.read(type[0], file.getInputStream(), 0);
        boolean read1 = EasyExcelHelper.read(type[1], file.getInputStream(), 1);
        boolean read2 = EasyExcelHelper.read(type[2], file.getInputStream(), 2);
        if (!(BooleanUtil.and(new boolean[]{read, read1, read2}))) {
            // 有一个错误则处理
            WriteSheet writeSheet1 = EasyExcelHelper.writeSheet(UserExcelReq.class, 0, "用户1");
            WriteSheet writeSheet2 = EasyExcelHelper.writeSheet(UserExcelReq.class, 1, "用户2");
            WriteSheet writeSheet3 = EasyExcelHelper.writeSheet(UserExcelReq.class, 2, "用户3");

            List<WriteSheetModel> writeSheetModels = Lists.newArrayList();

            Cache<String, String> instance = CacheManager.getInstance();
            List<UserExcelReq> userExcelRes1 = FastJsonUtil.toList(instance.get(type[0]), UserExcelReq.class);
            writeSheetModels.add(WriteSheetModel.builder().writeSheet(writeSheet1).data(userExcelRes1).build());

            List<UserExcelReq> userExcelRes2 = FastJsonUtil.toList(instance.get(type[1]), UserExcelReq.class);
            writeSheetModels.add(WriteSheetModel.builder().writeSheet(writeSheet2).data(userExcelRes2).build());

            List<UserExcelReq> userExcelRes3 = FastJsonUtil.toList(instance.get(type[2]), UserExcelReq.class);
            writeSheetModels.add(WriteSheetModel.builder().writeSheet(writeSheet3).data(userExcelRes3).build());

            EasyExcelHelper.writeExcel(response, "用户错误信息", writeSheetModels);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/export")
    public ResponseEntity export(@RequestParam long count, HttpServletResponse response) {

        // 1
        EasyExcelHelper.writeExcel(response, UserExcelRes.class, this.getList1(count));

        // 2
        WriteSheet writeSheet = EasyExcel.writerSheet(0, "用户1")
                .head(UserExcelRes.class)
                .registerConverter(new UserConverter())
                .build();
        EasyExcelHelper.writeExcel(response, "用户信息", this.getList1(count), writeSheet);

        // 3
        EasyExcelHelper.writeExcel(response, head(), 0, "测试导出sheet1", "测试导出", contentData());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/batchExport")
    public void batchExport(HttpServletResponse response) {

        WriteSheet writeSheet1 = EasyExcelHelper.writeSheet(UserExcelRes.class, 0, "用户1");

        WriteSheet writeSheet2 = EasyExcelHelper.writeSheet(UserExcelRes.class, 1, "用户2");

        WriteSheet writeSheet3 = EasyExcelHelper.writeSheet(UserExcelRes.class, 2, "用户3");

        List<WriteSheetModel> writeSheetModels = Lists.newArrayList();

        BatchFetchExecuteAdaptor<String, UserExcelRes> batchFetchExecuteAdaptor1 = new BatchFetchExecuteAdaptor<String, UserExcelRes>() {
            @Override
            protected List<UserExcelRes> doExecutePage(Integer current, Integer size, Object otherArg) {
                Map<String, Object> map = MapUtil.<String, Object>builder()
                        .put("pageNum", current)
                        .put("pageSize", size)
                        .build();
                return getList1(map);
            }
        };
        writeSheetModels.add(
                WriteSheetModel.builder()
                        .data(batchFetchExecuteAdaptor1.partFetchExecuteByPage(1000, null))
                        .writeSheet(writeSheet1)
                        .build()
        );

        BatchFetchExecuteAdaptor<String, UserExcelRes> batchFetchExecuteAdaptor2 = new BatchFetchExecuteAdaptor<String, UserExcelRes>() {
            @Override
            protected List<UserExcelRes> doExecutePage(Integer current, Integer size, Object otherArg) {
                Map<String, Object> map = MapUtil.<String, Object>builder()
                        .put("pageNum", current)
                        .put("pageSize", size)
                        .build();
                return getList2(map);
            }
        };
        writeSheetModels.add(
                WriteSheetModel.builder()
                        .data(batchFetchExecuteAdaptor2.partFetchExecuteByPage(1000, null))
                        .writeSheet(writeSheet2)
                        .build()
        );

        BatchFetchExecuteAdaptor<String, UserExcelRes> batchFetchExecuteAdaptor3 = new BatchFetchExecuteAdaptor<String, UserExcelRes>() {
            @Override
            protected List<UserExcelRes> doExecutePage(Integer current, Integer size, Object otherArg) {
                Map<String, Object> map = MapUtil.<String, Object>builder()
                        .put("pageNum", current)
                        .put("pageSize", size)
                        .build();
                return getList3(map);
            }
        };
        writeSheetModels.add(
                WriteSheetModel.builder()
                        .data(batchFetchExecuteAdaptor3.partFetchExecuteByPage(1000, null))
                        .writeSheet(writeSheet3)
                        .build()
        );
        EasyExcelHelper.writeExcel(response, "用户信息", writeSheetModels);
    }

    private List<UserExcelRes> getList1(Map<String, Object> map) {
        Long pageSize = Convert.toLong(map.get("pageSize"));
        Long pageNum = Convert.toLong(map.get("pageNum"));
        return getList1(5000)
                .stream()
                .skip((pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    private List<UserExcelRes> getList2(Map<String, Object> map) {
        Long pageSize = Convert.toLong(map.get("pageSize"));
        Long pageNum = Convert.toLong(map.get("pageNum"));
        return getList2(5000)
                .stream()
                .skip((pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    private List<UserExcelRes> getList3(Map<String, Object> map) {
        Long pageSize = Convert.toLong(map.get("pageSize"));
        Long pageNum = Convert.toLong(map.get("pageNum"));
        return getList3(5000)
                .stream()
                .skip((pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    private List<UserExcelRes> getList1(long count) {
        List<UserExcelRes> dtoList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            dtoList.add(UserExcelRes.builder()
                    .userName("张三" + i)
                    .userId((long) i)
                    .account("aaa" + i)
                    .build());
        }
        return dtoList;
    }

    private List<UserExcelRes> getList2(long count) {
        List<UserExcelRes> dtoList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            dtoList.add(UserExcelRes.builder()
                    .userName("李四" + i)
                    .userId((long) i)
                    .account("bbb" + i)
                    .build());
        }
        return dtoList;
    }

    private List<UserExcelRes> getList3(long count) {
        List<UserExcelRes> dtoList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            dtoList.add(UserExcelRes.builder()
                    .userName("王五" + i)
                    .userId((long) i)
                    .account("ccc" + i)
                    .build());
        }
        return dtoList;
    }

    /**
     * 生成动态表头，表头数据可以当做参数传入
     *
     * @return 表头list
     */
    private static List<List<String>> head() {
        List<List<String>> headTitles = Lists.newArrayList();
//        String warZone = "战区", base = "基地", personal = "个人", empty = " ", total = "合计", invoiceAmount = "开票额(亿元)", invoiceQuantity = "开票量(吨)", subtotal = "小计";
        String warZone = "表头1", base = "表头2", personal = "表头3", total = "合计", invoiceAmount = "子项1", invoiceQuantity = "子项2", subtotal = "小计";

        //第一列，1/2/3行
        headTitles.add(Lists.newArrayList(warZone));
        headTitles.add(Lists.newArrayList(base));
        headTitles.add(Lists.newArrayList(personal));
        headTitles.add(Lists.newArrayList(total, invoiceAmount, invoiceAmount));
        headTitles.add(Lists.newArrayList(total, invoiceQuantity, invoiceQuantity));

        // 可动态获取
        List<String> channelList = Lists.newArrayList("动态渠道1", "动态渠道2");
        // 可动态获取
        List<String> orderDetailed = Lists.newArrayList(subtotal, "order1", "order2", "order3");
        channelList.forEach(channel -> {
            orderDetailed.forEach(title -> {
                headTitles.add(Lists.newArrayList(channel, title, invoiceAmount, invoiceAmount));
                headTitles.add(Lists.newArrayList(channel, title, invoiceQuantity, invoiceQuantity));
            });
        });
        return headTitles;
    }

    /**
     * 导入数据封装，需要导出数据进行传参
     *
     * @return 导出数据集合
     */
    private static List<List<Object>> contentData() {
        List<List<Object>> contentList = Lists.newArrayList();
        // 这里一个List<Object>代表一行数据，需要映射成每行数据填充，横向填充（把实体数据的字段设置成一个List<Object>）
        contentList.add(Lists.newArrayList("测试", "测试A", "测试B", "100", 999, 999, 666.66, "200", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试"));
        contentList.add(Lists.newArrayList("测试", "测试A1", "测试B1", "2002", 888, 888, 888.88, "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试", "测试"));
        return contentList;
    }

}
