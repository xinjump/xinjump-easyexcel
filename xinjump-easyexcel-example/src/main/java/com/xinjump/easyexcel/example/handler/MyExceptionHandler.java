package com.xinjump.easyexcel.example.handler;

import com.alibaba.excel.exception.ExcelAnalysisException;
import com.xinjump.easyexcel.exception.ExcelNotifyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;

/**
 * @author Administrator
 */
@Slf4j
@RestControllerAdvice
public class MyExceptionHandler {

    @Order
    @ExceptionHandler(Throwable.class)
    public ResponseEntity handleException(Throwable e) {

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @ExceptionHandler({
            MaxUploadSizeExceededException.class
    })
    public ResponseEntity handleException(MaxUploadSizeExceededException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("上传超长");
    }

    @ExceptionHandler({
            ExcelNotifyException.class,
            ExcelAnalysisException.class
    })
    public ResponseEntity handleExcelException(Exception exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @ExceptionHandler({
            IllegalArgumentException.class,
            IllegalStateException.class
    })
    public ResponseEntity handleException(Exception exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleException(MethodArgumentNotValidException exception) {
        // 参数校验错误
        BindingResult result = exception.getBindingResult();
        List<ObjectError> list = result.getAllErrors();
        StringBuilder builder = new StringBuilder();
        if (list.size() > 0) {
            for (ObjectError error : list) {
                builder.append(error.getDefaultMessage()).append(", ");
            }
        }
        if (builder.length() > 0) {
            builder.delete(builder.length() - 2, builder.length());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(builder.toString());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleException(ConstraintViolationException exception) {
        // 参数校验错误
        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        for (ConstraintViolation<?> violation : constraintViolations) {
            String messageTemplate = violation.getMessageTemplate();
            builder.append(messageTemplate).append(", ");
        }
        if (builder.length() > 0) {
            builder.delete(builder.length() - 2, builder.length());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(builder.toString());
    }

}
