package com.xinjump.easyexcel.example.biz.service.impl;

import com.xinjump.easyexcel.example.biz.entity.UserEntity;
import com.xinjump.easyexcel.example.biz.service.UserService;
import com.xinjump.easyexcel.example.util.SpringContextUtil;
import com.xinjump.easyexcel.write.selected.ExcelDynamicSelect;

public class UserExcelSelectImpl implements ExcelDynamicSelect {

    @Override
    public String[] getSource() {
        UserService bean = SpringContextUtil.getBean(UserService.class);
        return bean.selectAll()
                .stream()
                .map(UserEntity::getUserName)
                .toArray(String[]::new);
    }

}
