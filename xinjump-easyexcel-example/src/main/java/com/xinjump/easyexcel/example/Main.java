package com.xinjump.easyexcel.example;

import com.xinjump.easyexcel.event.AnalysisEventAutoListener;
import com.xinjump.easyexcel.example.excel.UserEventListener;
import org.springframework.core.ResolvableType;

public class Main {
    public static void main(String[] args) {
        ResolvableType superType = ResolvableType.forClass(AnalysisEventAutoListener.class).getSuperType();
        Class<?> resolve = superType.getGeneric(0).resolve();
    }
}
