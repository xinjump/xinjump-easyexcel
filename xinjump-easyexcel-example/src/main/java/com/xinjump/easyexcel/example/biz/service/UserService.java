package com.xinjump.easyexcel.example.biz.service;

import com.xinjump.easyexcel.example.biz.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/16 14:55
 * @see com.xinjump.easyexcel.example.biz.service
 */
public interface UserService {

    void query();

    boolean saveOrUpdateBatch(List<UserEntity> userEntityList);

    List<UserEntity> selectAll();

}
