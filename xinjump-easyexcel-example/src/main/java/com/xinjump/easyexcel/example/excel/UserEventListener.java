package com.xinjump.easyexcel.example.excel;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.xinjump.easyexcel.ExcelResult;
import com.xinjump.easyexcel.annotation.ExcelListener;
import com.xinjump.easyexcel.cache.Cache;
import com.xinjump.easyexcel.cache.CacheManager;
import com.xinjump.easyexcel.event.AnalysisEventAutoListener;
import com.xinjump.easyexcel.example.biz.entity.UserEntity;
import com.xinjump.easyexcel.example.biz.model.req.UserExcelReq;
import com.xinjump.easyexcel.example.biz.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/11/20 14:58
 */
@ExcelListener({"userEventListener", "userEventListener2", "userEventListener3"})
public class UserEventListener extends AnalysisEventAutoListener<UserExcelReq> {

    @Autowired
    private UserService userService;

    public UserEventListener() {
        super(1000);
    }

    @Override
    protected ExcelResult<UserExcelReq> checkData(List<UserExcelReq> dataList) {
        List<UserExcelReq> successList = Lists.newArrayList();
        List<UserExcelReq> errorList = Lists.newArrayList();
        // 获取操作人
        HttpServletRequest request = this.getRequest();

        String errMsg;
        for (UserExcelReq userExcelReq : dataList) {
            errMsg = "";

            if (StringUtils.isBlank(errMsg)) {
                successList.add(userExcelReq);
            } else {
                userExcelReq.setValidateMessage(errMsg);
                errorList.add(userExcelReq);
            }
        }

        return new ExcelResult<>(successList, errorList);
    }

    @Override
    protected Function<List<UserExcelReq>, Object> storageDataFun() {
        return (userExcelReqs) -> {
            List<UserEntity> userEntityList = Lists.newArrayList();
            userExcelReqs.forEach(userExcelReq -> {
                UserEntity userEntity = new UserEntity();
                BeanUtils.copyProperties(userExcelReq, userEntity);
                userEntityList.add(userEntity);
            });
            return userService.saveOrUpdateBatch(userEntityList);
        };
    }

    @Override
    protected Consumer<List<UserExcelReq>> readErrorDataConsumer() {
        return (userExcelReqs) -> {
            // 错误数据处理
            Cache<String, String> instance = CacheManager.getInstance();
            instance.put(getType(), JSON.toJSONString(userExcelReqs));
        };
    }

}
