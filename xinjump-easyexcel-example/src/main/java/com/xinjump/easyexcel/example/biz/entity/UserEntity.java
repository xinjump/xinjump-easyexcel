package com.xinjump.easyexcel.example.biz.entity;

import lombok.Data;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/19 15:29
 * @see com.xinjump.easyexcel.example.biz.entity
 */
@Data
public class UserEntity {

    private Long userId;

    private String userName;

    private String account;

    private String password;

}
