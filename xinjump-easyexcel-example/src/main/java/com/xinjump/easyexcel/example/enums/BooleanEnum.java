package com.xinjump.easyexcel.example.enums;

import com.xinjump.easyexcel.enums.BaseEnum;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/25 15:49
 * @see com.xinjump.easyexcel.example.enums
 */
public enum BooleanEnum implements BaseEnum<String, String> {

    VALID("1", "是"),
    NOT_VALID("0", "否");

    BooleanEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    private String key;
    private String value;

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public String getValue() {
        return this.value;
    }

}
