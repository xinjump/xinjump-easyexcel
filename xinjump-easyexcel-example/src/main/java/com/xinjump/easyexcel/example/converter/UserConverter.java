package com.xinjump.easyexcel.example.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.xinjump.easyexcel.example.enums.BooleanEnum;
import com.xinjump.easyexcel.util.BaseEnumUtil;

/**
 * @author xinjump
 * @date 2022/3/15 15:13
 * @see com.xinjump.easyexcel.example.converter
 */
public class UserConverter implements Converter<String> {

    @Override
    public Class supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        return null;
    }

    @Override
    public CellData convertToExcelData(String value, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String fieldName = excelContentProperty.getField().getName();
        return new CellData<>(this.getCellValue(fieldName, value));
    }

    private String getCellValue(String fieldName, String value) {
        switch (fieldName) {
            case "password":
                return BaseEnumUtil.getValueByKey(value, BooleanEnum.class);
            default:
                return "";
        }
    }


}
