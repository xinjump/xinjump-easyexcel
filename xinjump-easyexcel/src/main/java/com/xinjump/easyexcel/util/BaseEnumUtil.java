package com.xinjump.easyexcel.util;

import cn.hutool.core.convert.Convert;
import com.google.common.collect.Maps;
import com.xinjump.easyexcel.enums.BaseEnum;

import java.util.Map;

/**
 * 通用枚举工具类(处理字段转义返回前端)
 *
 * @author xinjump
 * @date 2019年7月9日12:38:49
 */
public class BaseEnumUtil {

    public static <E extends BaseEnum> E[] checkToEnum(Class<E> enumClass) {
        if (!enumClass.isEnum()) {
            throw new IllegalArgumentException("该方法只支持枚举类型！");
        } else if (!BaseEnum.class.isAssignableFrom(enumClass)) {
            throw new IllegalArgumentException("该方法的枚举类，必须实现BaseEnum接口！");
        } else {
            return enumClass.getEnumConstants();
        }
    }

    public static <K, V, T extends BaseEnum<K, V>> String getStrValueByKey(K key, Class<T> clazz) {

        return Convert.toStr(getValueByKey(key, clazz));
    }

    public static <K, V, T extends BaseEnum<K, V>> String getStrKeyByValue(V value, Class<T> clazz) {
        return Convert.toStr(getKeyByValue(value, clazz));
    }

    /**
     * 根据value获取key
     *
     * @param value
     * @param clazz
     * @param <K>
     * @param <V>
     * @param <T>
     * @return
     */
    public static <K, V, T extends BaseEnum<K, V>> K getKeyByValue(V value, Class<T> clazz) {
        for (T each : clazz.getEnumConstants()) {
            if (each.getValue().equals(value)) {
                return each.getKey();
            }
        }
        return null;
    }

    /**
     * 根据key获取value
     *
     * @param key
     * @param clazz
     * @param <K>
     * @param <V>
     * @param <T>
     * @return
     */
    public static <K, V, T extends BaseEnum<K, V>> V getValueByKey(K key, Class<T> clazz) {
        for (T each : clazz.getEnumConstants()) {
            if (each.getKey().equals(key)) {
                return each.getValue();
            }
        }
        return null;
    }

    /**
     * 枚举类数据转Map
     *
     * @param clazz
     * @param <K>
     * @param <V>
     * @param <T>
     * @return
     */
    public static <K, V, T extends BaseEnum<K, V>> Map<K, V> getMap(Class<T> clazz) {
        Map<K, V> resultMap = Maps.newHashMap();
        for (T each : clazz.getEnumConstants()) {
            resultMap.put(each.getKey(), each.getValue());
        }
        return resultMap;
    }

}
