package com.xinjump.easyexcel.util;

import com.xinjump.easyexcel.annotation.Excel;
import com.xinjump.easyexcel.exception.ExcelException;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/21 14:16
 * @see com.xinjump.easyexcel.util
 */
@SuppressWarnings("unchecked")
public class ExcelUtil {

    public static <T> Class<T> getFirstClassByList(List<T> data) {
        if (CollectionUtils.isEmpty(data)) {
            throw new ExcelException("数据为空！");
        }

        return (Class<T>) data.get(0).getClass();
    }

    public static Excel getExcel(Class<?> clazz) {
        // 获取类excel文件名
        Excel excel = clazz.getAnnotation(Excel.class);
        if (excel == null) {
            throw new ExcelException(String.format("class [%s] @Excel Annotation is Empty", clazz.getSimpleName()));
        }
        return excel;
    }

    public static Excel getSuperExcel(Class<?> clazz) {
        // 获取类excel文件名
        Excel excel = clazz.getSuperclass().getAnnotation(Excel.class);
        if (excel == null) {
            throw new ExcelException(String.format("superclass [%s] @Excel Annotation is Empty", clazz.getSimpleName()));
        }
        return excel;
    }

}
