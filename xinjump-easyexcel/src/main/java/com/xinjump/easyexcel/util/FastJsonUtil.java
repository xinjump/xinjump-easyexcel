package com.xinjump.easyexcel.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.*;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/12/3 10:26
 * @see com.xinjump.easyexcel.util
 */
@Slf4j
public class FastJsonUtil extends JSON {

    private static SerializeConfig config;

    static {
        config = new SerializeConfig();
        SimpleDateFormatSerializer simpleDateFormatSerializer = new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss");
        config.put(Date.class, simpleDateFormatSerializer);
        config.put(java.sql.Date.class, simpleDateFormatSerializer);
    }

    private static SerializerFeature[] features = {
            SerializerFeature.DisableCircularReferenceDetect,//打开循环引用检测，JSONField(serialize = false)不循环
            SerializerFeature.WriteDateUseDateFormat,//默认使用系统默认 格式日期格式化
            SerializerFeature.WriteMapNullValue, //输出空置字段
            SerializerFeature.WriteNullListAsEmpty,//list字段如果为null，输出为[]，而不是null
            SerializerFeature.WriteNullNumberAsZero,// 数值字段如果为null，输出为0，而不是null
            SerializerFeature.WriteNullBooleanAsFalse,//Boolean字段如果为null，输出为false，而不是null
            SerializerFeature.WriteNullStringAsEmpty//字符类型字段如果为null，输出为""，而不是null
    };

    /**
     * 返回Json字符串里面包含的一个对象
     *
     * @param object       :{"city":"china","map1":[{"age":"28","name":"yys"}]}
     * @param childListKey :map1
     * @param clazz        :Map.class,或者其他对象
     * @param <T>          :Map
     * @return :List<Map>
     */
    public static <T> List<T> toChildList(Object object, String childListKey, Class<T> clazz) {
        return toChildList(object, childListKey, clazz, Lists.newArrayList());
    }

    /**
     * 返回Json字符串里面包含的一个对象
     *
     * @param object       :{"city":"china","map1":[{"age":"28","name":"yys"}]}
     * @param childListKey :map1
     * @param clazz        :Map.class,或者其他对象
     * @param <T>          :Map
     * @return :List<Map>
     */
    public static <T> List<T> toChildListAllowNull(Object object, String childListKey, Class<T> clazz) {
        return toChildList(object, childListKey, clazz, null);
    }

    /**
     * 返回Json字符串里面包含的一个对象（默认值）
     *
     * @param object       :{"city":"china","map1":[{"age":"28","name":"yys"}]}
     * @param childListKey :map1
     * @param clazz        :Map.class,或者其他对象
     * @param <T>          :Map
     * @return :List<Map>
     */
    public static <T> List<T> toChildList(Object object, String childListKey, Class<T> clazz, List<T> defaultObjs) {
        JSONObject jsonObj = obj2json(object);
        if (jsonObj == null) {
            return defaultObjs;
        }
        return toList(jsonObj.get(childListKey), clazz, defaultObjs);
    }

    /**
     * 返回Json字符串里面包含的一个对象
     *
     * @param object      :{"department":{"id":"1","name":"生产部"},"password":"admin","username":"admin"}
     * @param childObjKey :department
     * @param clazz       :Map.class,或者其他对象
     * @param <T>         :Map
     * @return
     */
    public static <T> T toChildObj(Object object, String childObjKey, Class<T> clazz) {
        return toChildObj(object, childObjKey, clazz, getClassInstance(clazz));
    }

    /**
     * 返回Json字符串里面包含的一个对象（允许为空）
     *
     * @param object      :{"department":{"id":"1","name":"生产部"},"password":"admin","username":"admin"}
     * @param childObjKey :department
     * @param clazz       :Map.class,或者其他对象
     * @param <T>         :Map
     * @return
     */
    public static <T> T toChildObjAllowNull(Object object, String childObjKey, Class<T> clazz) {
        return toChildObj(object, childObjKey, clazz, null);
    }

    /**
     * 返回Json字符串里面包含的一个对象（默认值）
     *
     * @param object      :{"department":{"id":"1","name":"生产部"},"password":"admin","username":"admin"}
     * @param childObjKey :department
     * @param clazz       :Map.class,或者其他对象
     * @param <T>         :Map
     * @return
     */
    public static <T> T toChildObj(Object object, String childObjKey, Class<T> clazz, T defaultObj) {
        JSONObject jsonObj = obj2json(object);
        if (jsonObj == null) {
            return defaultObj;
        }

        return toObj(jsonObj.get(childObjKey), clazz, defaultObj);
    }

    /**
     * json 转换成对象
     *
     * @param object
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObj(Object object, Class<T> clazz) {
        return toObj(object, clazz, getClassInstance(clazz));
    }

    /**
     * json 转换成对象（允许为空）
     *
     * @param object
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObjAllowNull(Object object, Class<T> clazz) {
        return toObj(object, clazz, null);
    }

    /**
     * json 转换成对象（默认值）
     *
     * @param object
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObj(Object object, Class<T> clazz, T defaultObj) {
        T result;
        if (object instanceof List) {
            result = toList(object, clazz).get(0);
        } else {
            result = parseObject(obj2jsonStr(object), clazz);
        }
        return ObjectUtils.isEmpty(result) ? defaultObj : result;
    }

    /**
     * json字符串转换成list
     *
     * @param object     object
     * @param clazz      要转换的class
     * @param <T>返回的泛型类型
     * @return 返回的<T>泛型类型
     */
    public static <T> List<T> toList(Object object, Class<T> clazz) {
        return toList(object, clazz, Lists.newArrayList());
    }

    /**
     * json字符串转换成list（允许为空）
     *
     * @param object     object
     * @param clazz      要转换的class
     * @param <T>返回的泛型类型
     * @return 返回的<T>泛型类型
     */
    public static <T> List<T> toListAllowNull(Object object, Class<T> clazz) {
        return toList(object, clazz, null);
    }

    /**
     * json字符串转换成list（默认值）
     *
     * @param object     object
     * @param clazz      要转换的class
     * @param <T>返回的泛型类型
     * @return 返回的<T>泛型类型
     */
    public static <T> List<T> toList(Object object, Class<T> clazz, List<T> defaultObjs) {
        // 如果不是集合对象，给object套壳
        if (Objects.nonNull(object) && !(object instanceof Collection)) {
            object = Collections.singletonList(object);
        }
        List<T> results = parseArray(obj2jsonStr(object), clazz);
        return CollectionUtils.isEmpty(results) ? defaultObjs : results;
    }

    /**
     * json字符串转换成Map
     *
     * @param object object
     * @return Map
     */
    public static Map<String, Object> toMap(Object object) {
        return toObjsAllowNull(object, new TypeReference<Map<String, Object>>() {
        });
    }

    /**
     * json 转换成list<map>
     *
     * @param object
     * @return
     */
    public static List<Map<String, Object>> toListMap(Object object) {
        return toObjsAllowNull(object, new TypeReference<List<Map<String, Object>>>() {
        });
    }

    /**
     * 对象转换成json字符串
     *
     * @param object 要转换的对象
     * @return 返回一个json 字符串数组
     */
    public static String obj2jsonStrByFeatures(Object object) {
        return toJSONString(object, config, features);
    }

    /**
     * 对象转换成json字符串
     *
     * @param object 要转换的对象
     * @return 返回一个json 字符串数组
     */
    public static String obj2jsonStr(Object object) {
        if (object instanceof String) {
            boolean flag = true;
            try {
                parse((String) object);
            } catch (Exception ignored) {
                flag = false;
            }
            if (flag) {
                return (String) object;
            }
        }
        return toJSONString(object, config);
    }

    /**
     * 对象转换成json对象
     *
     * @param object 要转换的对象
     * @return 返回一个json对象
     */
    public static JSONObject obj2json(Object object) {
        JSONObject result = null;
        try {
            result = parseObject(obj2jsonStr(object));
        } catch (Exception ignored) {
        }
        return result;
    }

    public static <T> T toObjsAllowNull(Object object, TypeReference<T> typeReference) {
        return toObjs(object, typeReference, null);
    }

    public static <T> T toObjs(Object object, TypeReference<T> typeReference, T defaultObj) {
        T result = parseObject(obj2jsonStr(object), typeReference);
        return ObjectUtils.isEmpty(result) ? defaultObj : result;
    }

    private static <T> T getClassInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error("获取实例异常：", e);
            throw new RuntimeException("获取实例异常");
        }
    }

}
