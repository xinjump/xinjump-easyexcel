package com.xinjump.easyexcel.helper;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.handler.WriteHandler;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.google.common.collect.Lists;
import com.xinjump.easyexcel.ExcelRow;
import com.xinjump.easyexcel.annotation.Excel;
import com.xinjump.easyexcel.event.AnalysisEventAutoListener;
import com.xinjump.easyexcel.event.ExcelManager;
import com.xinjump.easyexcel.exception.ExcelException;
import com.xinjump.easyexcel.util.ExcelUtil;
import com.xinjump.easyexcel.write.model.WriteSheetModel;
import com.xinjump.easyexcel.write.selected.SelectedSheetWriteHandler;
import com.xinjump.easyexcel.write.style.CustomColumnWidthStyleHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/9/20 17:04
 * @see com.xinjump.easyexcel.helper
 */
@SuppressWarnings({"rawtypes"})
@Slf4j
public final class EasyExcelHelper extends EasyExcelFactory {

    /**
     * 根据类型获取导入进度
     *
     * @param type
     * @return
     */
//    @Deprecated
//    public static String getProgressBar(String type) {
//        return Convert.toStr(CacheManager.getInstance().get(ExcelManager.getExcelClass(type).getName()));
//    }

    public static boolean read(String type, InputStream is) {
        return read(type, is, 0);
    }

    public static boolean read(String type, InputStream is, Integer sheetNo) {
        return read(type, is, sheetNo, null);
    }

    public static boolean read(String type, InputStream is, Integer sheetNo, HttpServletRequest request) {
        return read(type, is, sheetNo, request, null);
    }

    /**
     * @param type     指定导入执行的EventListener
     * @param is       文件流
     * @param sheetNo  指定读取sheet
     * @param request  获取请求信息（例如登录人）
     * @param response 如果有错误直接下载错误模板
     */
    public static boolean read(String type, InputStream is, Integer sheetNo, HttpServletRequest request, HttpServletResponse response) {
        return read(is, sheetNo, ExcelManager.getEventListener(type, request, response));
    }

    public static boolean read(InputStream is, AnalysisEventAutoListener listener) {
        return read(is, 0,  listener);
    }

    public static boolean read(InputStream is, Integer sheetNo, AnalysisEventAutoListener listener) {
        boolean flag = true;
        try {
            read(is, listener.getClazz(), listener).sheet(sheetNo).doRead();
        } catch (ExcelException e) {
            flag = false;
        }
        return flag;
    }

    public static <T> void downloadTemplate(HttpServletResponse response,
                                            Class<T> clazz,
                                            List<T> data) {
        Excel excel = ExcelUtil.getExcel(clazz);
        List<List<String>> heads = Arrays.stream(clazz.getDeclaredFields())
                .map(e -> e.getAnnotation(ExcelProperty.class))
                .sorted(Comparator.comparingInt(ExcelProperty::index))
                .map(excelProperty -> Lists.newArrayList(excelProperty.value()))
                .collect(Collectors.toList());
        writeExcel(response, clazz, heads, 0, excel.sheetName(), excel.fileName(), data,
                new CustomColumnWidthStyleHandler(), new SelectedSheetWriteHandler(clazz));
    }

    public static <T extends ExcelRow> void downloadSingleError(HttpServletResponse response,
                                                                Class<T> clazz,
                                                                List<T> example) {
        Excel excel = ExcelUtil.getExcel(clazz);
        Excel superExcel = ExcelUtil.getSuperExcel(clazz);
        String sheetName = StringUtils.isBlank(excel.sheetName()) ? excel.fileName() : excel.sheetName() + superExcel.sheetName();
        String fileName = excel.fileName() + superExcel.fileName();

        WriteSheet writeSheet = writerSheet(0, sheetName)
                .useDefaultStyle(true)
                .head(clazz)
                .registerWriteHandler(new CustomColumnWidthStyleHandler())
                .registerWriteHandler(new SelectedSheetWriteHandler(clazz))
                .build();
        writeExcel(response, fileName, example, writeSheet);
    }

    public static <T> void writeExcel(HttpServletResponse response,
                                      List<List<String>> head,
                                      Integer sheetNo,
                                      String sheetName,
                                      String fileName,
                                      List<T> data) {
        writeExcel(response, null, head, sheetNo, sheetName, fileName, data, new CustomColumnWidthStyleHandler());
    }

    public static <T> void writeExcel(HttpServletResponse response,
                                      Class<T> clazz,
                                      List<T> data) {
        writeExcel(response, clazz, data, new CustomColumnWidthStyleHandler(), new SelectedSheetWriteHandler(clazz));
    }

    /**
     * 导出Excel到web
     *
     * @param response      响应
     * @param data          要导出的数据
     * @param writeHandlers writeHandlers
     */
    public static <T> void writeExcel(HttpServletResponse response,
                                      Class<T> clazz,
                                      List<T> data,
                                      WriteHandler... writeHandlers) {
        Excel excel = ExcelUtil.getExcel(clazz);
        writeExcel(response, clazz, null, 0, excel.sheetName(), excel.fileName(), data, writeHandlers);
    }

    public static <T> void writeExcel(HttpServletResponse response,
                                      Class<T> clazz,
                                      List<List<String>> head,
                                      Integer sheetNo,
                                      String sheetName,
                                      String fileName,
                                      List<T> data,
                                      WriteHandler... writeHandlers) {
        ExcelWriter excelWriter = excelWriter(response, fileName);
        WriteSheet writeSheet = writeSheet(clazz, head, sheetNo, sheetName, writeHandlers);
        excelWriter.write(CollectionUtils.isEmpty(data) ? Lists.newArrayList() : data, writeSheet);
        excelWriter.finish();
    }

    public static <T> void writeExcel(HttpServletResponse response,
                                      String fileName,
                                      List<T> data,
                                      WriteSheet writeSheet) {
        ExcelWriter excelWriter = excelWriter(response, fileName);
        excelWriter.write(data, writeSheet);
        excelWriter.finish();
    }

    public static void writeExcel(HttpServletResponse response,
                                  String fileName,
                                  List<WriteSheetModel> writeSheetModels) {
        ExcelWriter excelWriter = excelWriter(response, fileName);
        for (WriteSheetModel writeSheetModel : writeSheetModels) {
            excelWriter.write(writeSheetModel.getData(), writeSheetModel.getWriteSheet());
        }
        excelWriter.finish();
    }

    /**
     * 创建即将导出的sheet页（sheet页中含有带下拉框的列）
     *
     * @param clazz     导出的表头信息和配置
     * @param sheetNo   sheet索引
     * @param sheetName sheet名称
     * @param <T>       泛型
     * @return sheet页
     */
    public static <T> WriteSheet writeSheet(Class<T> clazz,
                                            Integer sheetNo,
                                            String sheetName) {
        return writeSheet(clazz, null, sheetNo, sheetName,
                new CustomColumnWidthStyleHandler(), new SelectedSheetWriteHandler(clazz));
    }

    public static <T> ExcelWriterSheetBuilder writeSheetBuilder(Class<T> clazz,
                                                                Integer sheetNo,
                                                                String sheetName) {
        return writeSheetBuilder(clazz, null, sheetNo, sheetName,
                new CustomColumnWidthStyleHandler(), new SelectedSheetWriteHandler(clazz));
    }

    public static WriteSheet writeSheet(List<List<String>> head,
                                        Integer sheetNo,
                                        String sheetName) {
        return writeSheet(null, head, sheetNo, sheetName, new CustomColumnWidthStyleHandler());
    }

    public static ExcelWriterSheetBuilder writeSheetBuilder(List<List<String>> head,
                                                            Integer sheetNo,
                                                            String sheetName) {
        return writeSheetBuilder(null, head, sheetNo, sheetName, new CustomColumnWidthStyleHandler());
    }

    public static <T> WriteSheet writeSheet(Class<T> clazz,
                                            List<List<String>> head,
                                            Integer sheetNo,
                                            String sheetName,
                                            WriteHandler... writeHandlers) {
        return writeSheetBuilder(clazz, head, sheetNo, sheetName, writeHandlers).build();
    }

    public static <T> ExcelWriterSheetBuilder writeSheetBuilder(Class<T> clazz,
                                                                List<List<String>> head,
                                                                Integer sheetNo,
                                                                String sheetName,
                                                                WriteHandler... writeHandlers) {
        ExcelWriterSheetBuilder excelWriterSheetBuilder = writerSheet(sheetNo, sheetName);
        if (CollectionUtils.isEmpty(head)) {
            excelWriterSheetBuilder.head(clazz);
        } else {
            excelWriterSheetBuilder.head(head);
        }
        for (WriteHandler writeHandler : writeHandlers) {
            excelWriterSheetBuilder.registerWriteHandler(writeHandler);
        }
        return excelWriterSheetBuilder;
    }

    public static ExcelWriter excelWriter(HttpServletResponse response, String fileName) {

        return write(OutputStream.apply(response, fileName)).build();
    }

    public static BiFunction<HttpServletResponse, String, OutputStream> OutputStream = (response, fileName) -> {
        try {
            String finalFileName = URLEncoder.encode((fileName + ExcelTypeEnum.XLSX.getValue()), "UTF-8");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");
            response.setHeader("Content-Disposition", "attachment;filename=" + finalFileName);
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            return response.getOutputStream();
        } catch (IOException e) {
            throw new ExcelException("获取文件失败");
        }
    };

}
