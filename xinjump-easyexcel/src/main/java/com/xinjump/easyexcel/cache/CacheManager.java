package com.xinjump.easyexcel.cache;

import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021年3月24日10:49:51
 * @see com.xinjump.easyexcel.cache
 */
@Slf4j
public class CacheManager<K, V> implements Cache<K, V>, Cloneable, Serializable {
    /**
     * 键值对集合
     */
    private final Map<K, CacheEntity<V>> cache = new ConcurrentHashMap<>();
    /**
     * 定时器线程池，用于清除过期缓存
     */
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    /**
     * 定义实例
     */
    private static CacheManager cacheManager = null;

    @SuppressWarnings("unchecked")
    public static <K, V> Cache<K, V> getInstance() {
        if (cacheManager == null) {
            synchronized (CacheManager.class) {
                if (cacheManager == null) {
                    cacheManager = new CacheManager<>();
                }
            }
        }
        return cacheManager;
    }

    @Override
    public synchronized int size() {
        return cache.size();
    }

    @Override
    public synchronized boolean isEmpty() {
        return cache.isEmpty();
    }

    @Override
    public synchronized boolean containsKey(K key) {
        return cache.containsKey(key);
    }

    @Override
    public synchronized boolean containsValue(V value) {
        Collection<V> values = this.values();
        for (V v : values) {
            if (value != null && value.equals(v)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized V get(K key) {
        CacheEntity<V> cacheEntity = cache.get(key);
        if (cacheEntity != null) {
            return cacheEntity.getValue();
        }
        return null;
    }

    @Override
    public synchronized V put(K key, V value) {
        return this.put(key, value, 0);
    }

    @Override
    public synchronized V put(K key, V value, long expire) {
        // 清除原键值对
        if (this.containsKey(key)) {
            V rv = this.remove(key);
//            log.info("清除原键值为：{}", rv);
        }
        //设置过期时间(毫秒)
        if (expire > 0) {
            Future<?> future = executor.schedule(() -> {
                // 过期后清除该键值对
                synchronized (this) {
                    cache.remove(key);
                }
            }, expire, TimeUnit.MILLISECONDS);
            cache.put(key, new CacheEntity<>(value, future));
        } else {
            cache.put(key, new CacheEntity<>(value, null));
        }
        return value;
    }

    @Override
    public synchronized V remove(K key) {
        // 清除原缓存数据
        CacheEntity<V> cacheEntity = cache.remove(key);
        if (cacheEntity == null) {
            return null;
        }
        // 清除原键值对定时器
        Future future = cacheEntity.getFuture();
        if (future != null) {
            future.cancel(true);
        }
        return cacheEntity.getValue();
    }

    @Override
    public synchronized void clear() {
        cache.clear();
    }

    @Override
    public synchronized Set<K> keySet() {
        return cache.keySet();
    }

    @Override
    public synchronized Collection<V> values() {
        return cache.values().stream().map(CacheEntity::getValue).collect(Collectors.toList());
    }

}
