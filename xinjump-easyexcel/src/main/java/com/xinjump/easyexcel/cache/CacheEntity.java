package com.xinjump.easyexcel.cache;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.Future;

@Getter
@AllArgsConstructor
public class CacheEntity<V> {
    /**
     * 键值对的value
     */
    private V value;
    /**
     * 定时器Future
     */
    private Future<?> future;

}
