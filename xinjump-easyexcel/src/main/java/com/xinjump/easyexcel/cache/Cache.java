package com.xinjump.easyexcel.cache;

import java.util.Collection;
import java.util.Set;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021年3月24日10:49:02
 * @see com.xinjump.easyexcel.cache
 */
public interface Cache<K, V> {

    int size();

    boolean isEmpty();

    boolean containsKey(K key);

    boolean containsValue(V value);

    V get(K key);

    V put(K key, V value);

    V put(K key, V value, long expire);

    default V putIfAbsent(K key, V value, long expire) {
        V v = get(key);
        if (v == null) {
            v = put(key, value, expire);
        }
        return v;
    }

    V remove(K key);

    default void remove(K... keys) {
        for (K key : keys) {
            remove(key);
        }
    }

    void clear();

    Set<K> keySet();

    Collection<V> values();

}
