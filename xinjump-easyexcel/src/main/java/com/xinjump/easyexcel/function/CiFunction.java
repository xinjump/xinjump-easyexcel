package com.xinjump.easyexcel.function;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/14 16:25
 * @see com.xinjump.easyexcel.function
 */
@FunctionalInterface
public interface CiFunction<T, U, O, R> {

    R apply(T t, U u, O o);

}
