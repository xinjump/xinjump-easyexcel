package com.xinjump.easyexcel.enums;

import com.xinjump.easyexcel.util.BaseEnumUtil;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/21 18:03
 * @see com.xinjump.easyexcel.enums
 */
public interface BaseEnum<K, V> {

    K getKey();

    V getValue();

    static <E extends BaseEnum> E keyOf(Class<E> enumClass, Object key) {
        E[] es = BaseEnumUtil.checkToEnum(enumClass);

        for (E item : es) {
            if (item.getKey().equals(key)) {
                return item;
            }
        }

        throw new IllegalStateException("无法识别的枚举值：" + key + "(" + enumClass.getName() + ")");
    }

    static <E extends BaseEnum> E valueOf(Class<E> enumClass, Object value) {
        E[] es = BaseEnumUtil.checkToEnum(enumClass);

        for (E item : es) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }

        throw new IllegalStateException("无法识别的枚举值：" + value + "(" + enumClass.getName() + ")");
    }

}
