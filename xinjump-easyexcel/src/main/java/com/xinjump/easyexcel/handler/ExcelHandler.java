//package com.xinjump.easyexcel.handler;
//
//import cn.hutool.core.convert.Convert;
//import com.xinjump.easyexcel.cache.CacheManager;
//import com.xinjump.easyexcel.event.ExcelManager;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author xinjump
// * @version 1.1
// * @date 2020/12/14 17:27
// * @see com.xinjump.easyexcel.handler
// */
//public interface ExcelHandler {
//
//    default void download(String type, HttpServletResponse response) throws Exception {
//        this.download(type, response, new ArrayList<>());
//    }
//
//    void download(String type, HttpServletResponse response, List<?> example) throws Exception;
//
//    default void read(String type, InputStream is, HttpServletRequest request) throws Exception {
//        this.read(type, is, null, request, null);
//    }
//
//    default void read(String type, InputStream is, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        this.read(type, is, null, request, response);
//    }
//
//    @Deprecated
//    void read(String type, InputStream is, Integer sheetNo, HttpServletRequest request, HttpServletResponse response) throws Exception;
//
//    default String getProgressBar(String type) {
//        return Convert.toStr(CacheManager.getInstance().get(ExcelManager.getExcelClass(type).getName()));
//    }
//
//}
