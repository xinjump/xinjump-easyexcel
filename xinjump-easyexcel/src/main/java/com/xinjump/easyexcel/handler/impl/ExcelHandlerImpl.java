//package com.xinjump.easyexcel.handler.impl;
//
//import com.alibaba.excel.annotation.ExcelProperty;
//import com.xinjump.easyexcel.event.ExcelManager;
//import com.xinjump.easyexcel.handler.ExcelHandler;
//import com.xinjump.easyexcel.helper.EasyExcelHelper;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @author xinjump
// * @version 1.1
// * @date 2020/12/14 17:30
// * @see com.xinjump.easyexcel.handler.impl
// */
//public class ExcelHandlerImpl implements ExcelHandler {
//
//    @Override
//    public void download(String type, HttpServletResponse response, List<?> example) throws Exception {
//        Class<?> clazz = ExcelManager.getExcelClass(type);
//        List<List<String>> heads = Arrays.stream(clazz.getDeclaredFields())
//                .map(e -> e.getAnnotation(ExcelProperty.class))
//                .sorted(Comparator.comparingInt(ExcelProperty::index))
//                .map(excelProperty -> new ArrayList<>(Arrays.asList(excelProperty.value())))
//                .collect(Collectors.toList());
//        EasyExcelHelper.download(response, clazz, heads, example);
//    }
//
//    @Override
//    public void read(String type, InputStream is, Integer sheetNo, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        EasyExcelHelper.read(is, sheetNo, ExcelManager.getEventListener(type, request, response));
//    }
//
//}
