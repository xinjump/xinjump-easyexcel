package com.xinjump.easyexcel;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/16 17:02
 * @see com.xinjump.easyexcel
 */
@AllArgsConstructor
@Data
public class ExcelResult<T> {

    private List<T> successList;

    private List<T> errorList;

}
