package com.xinjump.easyexcel.exception;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/24 14:49
 * @see com.xinjump.easyexcel.exception
 */
public class ExcelNotifyException extends RuntimeException {

    public ExcelNotifyException(String message) {
        super(message);
    }

    public ExcelNotifyException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExcelNotifyException(Throwable cause) {
        super(cause);
    }

}
