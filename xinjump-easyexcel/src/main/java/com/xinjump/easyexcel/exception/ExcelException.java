package com.xinjump.easyexcel.exception;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/24 14:49
 * @see com.xinjump.easyexcel.exception
 */
public class ExcelException extends RuntimeException {

    public ExcelException(String message) {
        super(message);
    }

    public ExcelException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExcelException(Throwable cause) {
        super(cause);
    }

}
