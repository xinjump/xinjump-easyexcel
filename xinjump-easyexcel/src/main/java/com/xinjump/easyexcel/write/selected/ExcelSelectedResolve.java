package com.xinjump.easyexcel.write.selected;

import com.google.common.collect.Lists;
import com.xinjump.easyexcel.annotation.ExcelSelected;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

@Data
@Slf4j
public class ExcelSelectedResolve {
    /**
     * 下拉内容
     */
    private String[] source;

    /**
     * 设置下拉框的起始行，默认为第二行
     */
    private int firstRow;

    /**
     * 设置下拉框的结束行，默认为最后一行
     */
    private int lastRow;

    public String[] resolveSelectedSource(ExcelSelected excelSelected) {
        if (excelSelected == null) {
            return null;
        }

        // 获取固定下拉框的内容
        String[] source = excelSelected.source();
        if (source.length > 0) {
            return source;
        }

        // 获取动态下拉框的内容
        Class<? extends ExcelDynamicSelect>[] classes = excelSelected.sourceClass();
        if (classes.length > 0) {
            // 按顺序拼接内容
            List<String> dynamicSelectSources = Lists.newArrayList();
            for (Class<? extends ExcelDynamicSelect> clazz : classes) {
                try {
                    ExcelDynamicSelect excelDynamicSelect = clazz.newInstance();
                    String[] dynamicSelectSource = excelDynamicSelect.getSource();
                    if (dynamicSelectSource != null && dynamicSelectSource.length > 0) {
                        dynamicSelectSources.addAll(Arrays.asList(dynamicSelectSource));
                    }
                } catch (InstantiationException | IllegalAccessException e) {
                    log.error("解析动态下拉框数据异常", e);
                }
            }
            return dynamicSelectSources
                    .stream()
                    .filter(StringUtils::isNotBlank)
                    .distinct()
                    .toArray(String[]::new);
        }

        return null;
    }

}
