package com.xinjump.easyexcel.write.model;

import com.alibaba.excel.write.metadata.WriteSheet;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author xinjump
 * @date 2022/4/15 13:58
 * @see com.xinjump.easyexcel.write.model
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WriteSheetModel {

    private List<?> data;
    private WriteSheet writeSheet;

}
