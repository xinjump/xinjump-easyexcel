package com.xinjump.easyexcel.validate;

import com.xinjump.easyexcel.ExcelRow;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.Set;
import java.util.StringJoiner;

public class HibernateValidator {

    private static Validator validator = null;

    static {
        /*
            使用hibernate的注解来进行验证 failFast true仅仅返回第一条错误信息 false返回所有错误
         */
        synchronized (HibernateValidator.class) {
            if (validator == null) {
                validator = Validation.byProvider(org.hibernate.validator.HibernateValidator.class)
                        .configure()
                        .addProperty("hibernate.validator.fail_fast", "false")
                        .buildValidatorFactory().getValidator();
            }
        }
    }

    public static Validator getValidator() {
        return validator;
    }

    public static <T extends ExcelRow> void hibernateValidate(T t) {
        Set<ConstraintViolation<T>> validateSet = getValidator().validate(t, Default.class);
        if (CollectionUtils.isNotEmpty(validateSet)) {
            StringJoiner result = new StringJoiner(" ");
            for (ConstraintViolation<T> violation : validateSet) {
                String message = violation.getMessage();
                if (StringUtils.isBlank(message)) {
                    continue;
                }
                result.add(message);
            }

            t.setValidateMessage(result.toString());
        }
    }

}
