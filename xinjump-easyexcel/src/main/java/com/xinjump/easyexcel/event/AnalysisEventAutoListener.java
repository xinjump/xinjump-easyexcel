package com.xinjump.easyexcel.event;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.xinjump.easyexcel.ExcelResult;
import com.xinjump.easyexcel.ExcelRow;
import com.xinjump.easyexcel.exception.ExcelException;
import com.xinjump.easyexcel.exception.ExcelNotifyException;
import com.xinjump.easyexcel.helper.EasyExcelHelper;
import com.xinjump.easyexcel.validate.HibernateValidator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ResolvableType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/11/1 16:21
 * @see com.xinjump.easyexcel.event
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Getter(value = AccessLevel.PUBLIC)
@Slf4j
public abstract class AnalysisEventAutoListener<T extends ExcelRow> extends AnalysisEventListener<T> {

    // 模板数据
    private final List<T> rows = Lists.newArrayList();
    // 模板中错误数据，执行完毕可以选择抛出所有错误 或者 导出错误
    private final List<T> errorList = Lists.newArrayList();

    // 设置当前处理的excel实体
    private final Class<T> clazz;
    // 设置多少条存储一次数据库
    private final int BATCH_COUNT;

    @Setter
    private HttpServletResponse response;
    @Setter
    private HttpServletRequest request;
    @Setter
    private String type;

    private final List<String> validHead;

    public AnalysisEventAutoListener(int BATCH_COUNT) {
        this(BATCH_COUNT, null, null);
    }

    public AnalysisEventAutoListener(int BATCH_COUNT, List<String> validHead) {
        this(BATCH_COUNT, null, validHead);
    }

    public AnalysisEventAutoListener(int BATCH_COUNT, HttpServletResponse response) {
        this(BATCH_COUNT, response, null);
    }

    public AnalysisEventAutoListener(int BATCH_COUNT, HttpServletResponse response, List<String> validHead) {
        this.BATCH_COUNT = BATCH_COUNT;
        this.clazz = this.getEventListenerGenericsClass();
        this.response = response;
        this.validHead = validHead;
    }

    @SneakyThrows
    @Override
    public void onException(Exception exception, AnalysisContext analysisContext) {
        log.error("解析失败，继续解析下一行:{}", exception.getMessage());
        if (exception instanceof ExcelNotifyException) {
            super.onException(exception, analysisContext);
        }
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException) exception;
            int perform = excelDataConvertException.getRowIndex() + 1;
            int column = excelDataConvertException.getColumnIndex() + 1;
            log.error("第{}行，第{}列解析异常{}，内容为：{}",
                    perform, column, excelDataConvertException.getCause().getMessage(), excelDataConvertException.getCellData());
        }
    }

    @Override
    public void invokeHead(Map<Integer, CellData> headMap, AnalysisContext analysisContext) {
        System.err.println();
        System.err.println();
        this.validHead(headMap);
        log.info("解析到一条头数据:{}", headMap);
        String simpleName = this.getEventListenerSimpleName();
        log.info("[{}]事件Excel共{}条数据", simpleName, analysisContext.getTotalCount() - 1);
    }

    /**
     * 校验头信息
     *
     * @param headMap
     */
    private void validHead(Map<Integer, CellData> headMap) {
        boolean isValidHead = true;
        for (int i = 0; i < validHead.size(); i++) {
            String head = validHead.get(i);
            String excelHead = headMap.get(i).getStringValue();
            if (!head.equals(excelHead)) {
                isValidHead = false;
                break;
            }
        }
        if (!isValidHead) {
            throw new ExcelNotifyException("Excel模板不合法");
        }
    }

    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        // 基本检验
        HibernateValidator.hibernateValidate(data);
        data.setRowNum(analysisContext.getCurrentRowNum() + 1);

        if (StringUtils.isBlank(data.getValidateMessage())) {
            this.getRows().add(data);
        } else {
            this.getErrorList().add(data);
        }

        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (this.getRows().size() >= this.getBATCH_COUNT()) {
            log.info("[{}]事件解析到{}条数据:{}", this.getEventListenerSimpleName(), this.getBATCH_COUNT(), JSON.toJSONString(this.getRows()));
            this.checkAndStorageData();
        }
        // 更新缓存进度
        if (analysisContext.getCurrentRowNum() % this.getBATCH_COUNT() != 0) {
            this.progress(analysisContext, false);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        try {
            if (CollectionUtils.isNotEmpty(this.getRows())) {
                this.checkAndStorageData();
            }

            if (CollectionUtils.isNotEmpty(this.getErrorList())) {
                log.info("[{}]事件Excel共{}条错误数据导入失败", this.getEventListenerSimpleName(), this.getErrorList().size());
                if (this.getResponse() == null) {
                    this.readErrorDataConsumer().accept(this.getErrorList());
                } else {
                    EasyExcelHelper.downloadSingleError(this.getResponse(), this.getClazz(), this.getErrorList());
                }

                throw new ExcelException("导入失败");
            }
        } finally {
            // 更新最后缓存进度
            this.progress(analysisContext, true);
            // 执行完毕；清空异常列
            this.getErrorList().clear();
        }
    }

    @Override
    public boolean hasNext(AnalysisContext analysisContext) {
        return true;
    }

    protected abstract ExcelResult<T> checkData(List<T> rows);

    // 设置调用存储数据层接口
    protected abstract Function<List<T>, Object> storageDataFun();

    // 导入有错误数据时函数消费
    protected Consumer<List<T>> readErrorDataConsumer() {
        return (t) -> {
            String messages = t
                    .stream()
                    .map(e -> String.format("第%s行数据错误；错误原因：【%s】", e.getRowNum(), e.getValidateMessage()))
                    .collect(Collectors.joining("\n"));
            throw new ExcelNotifyException(messages);
        };
    }

    /**
     * 检查和存储数据
     */
    private void checkAndStorageData() {
        try {
            ExcelResult<T> excelResult = this.checkData(this.getRows());
            if (Objects.isNull(excelResult)) {
                throw new ExcelNotifyException(String.format("程序出错，请管理员检查%s.checkData()方法返回对象", this.getEventListenerSimpleName()));
            }
            // 导入数据
            List<T> successList = excelResult.getSuccessList();
            if (CollectionUtils.isNotEmpty(successList)) {
                Object r = this.storageDataFun().apply(successList);
                log.info("[{}]事件{}条有效数据存储完毕，返回结果：{}", this.getEventListenerSimpleName(), successList.size(), r);
            }
            // 错误数据汇总
            this.getErrorList().addAll(excelResult.getErrorList());
        } finally {
            // 存储完成清理list
            this.getRows().clear();
        }
    }

    /**
     * 更新进度
     *
     * @param analysisContext analysisContext
     */
    private void progress(AnalysisContext analysisContext, boolean isFinally) {
        String totalCount = Convert.toStr(analysisContext.getTotalCount() - 1);
        String currentRowNum = Convert.toStr(analysisContext.getCurrentRowNum());

//        Cache<String, Object> cache = CacheManager.getInstance();
//        // 计算进度数
//        int progressNum = NumberUtil.mul(NumberUtil.div(currentRowNum, totalCount), 100).intValue();
//        // 缓存设定5分钟
//        cache.put(this.getClazz().getName(), (progressNum + "%"), (1000 * 60 * 5));

        // 获取excel剩余条数
        int residueCount = NumberUtil.sub(totalCount, currentRowNum).intValue();
        String simpleName = this.getEventListenerSimpleName();
        if (residueCount == 0 && isFinally) {
            log.info("[{}]事件Excel数据导入完毕，成功{}条；失败{}条",
                    simpleName,
                    NumberUtil.sub(totalCount, Convert.toStr(this.getErrorList().size())).intValue(),
                    this.getErrorList().size());
        } else {
            log.info("[{}]事件Excel剩余{}条数据", simpleName, residueCount);
        }
    }

    private String getEventListenerSimpleName() {
        return this.getClass().getSimpleName();
    }

    private Class<T> getEventListenerGenericsClass() {
        ResolvableType superType = ResolvableType.forClass(this.getClass()).getSuperType();
        return (Class<T>) superType.getGeneric(0).resolve();
    }

}
