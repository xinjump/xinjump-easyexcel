package com.xinjump.easyexcel.event;

import com.xinjump.easyexcel.annotation.ExcelListener;
import com.xinjump.easyexcel.exception.ExcelException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/1/19 11:10
 * @see com.xinjump.easyexcel.event
 */
@Slf4j
public class ExcelManager implements ApplicationContextAware, InitializingBean {

    private String[] packPaths;

    private static ApplicationContext applicationContext;

    private static Map<String, AnalysisEventAutoListener> map = new HashMap<>();

    public ExcelManager(String packPaths) {
        if (StringUtils.isBlank(packPaths)) {
            throw new ExcelException("请指定excel监听实例路径");
        }
        this.packPaths = packPaths.split(",");
    }

    public static Class<?> getExcelClass(String type) {
        return getEventListener(type).getClazz();
    }

    public static AnalysisEventAutoListener getEventListener(String type) {
        return getEventListener(type, null, null);
    }

    public static AnalysisEventAutoListener getEventListener(String type, HttpServletRequest request, HttpServletResponse response) {
        if (map.containsKey(type)) {
            AnalysisEventAutoListener analysisEventAutoListener = map.get(type);
            analysisEventAutoListener.setResponse(response);
            analysisEventAutoListener.setRequest(request);
            analysisEventAutoListener.setType(type);
            return analysisEventAutoListener;
        }
        throw new ExcelException(String.format("参数不合法，没有[%s]对应的监听类", type));
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (String prefix : this.packPaths) {
            Set<Class<?>> classSet = new Reflections(prefix).getTypesAnnotatedWith(ExcelListener.class);
            for (Class<?> c : classSet) {
                if (!c.isAssignableFrom(AnalysisEventAutoListener.class)) {
                    AnalysisEventAutoListener analysisEventAutoListener = (AnalysisEventAutoListener) c.newInstance();
                    // 注入spring容器
                    applicationContext.getAutowireCapableBeanFactory().autowireBean(analysisEventAutoListener);
                    String[] excelListenerValues = c.getAnnotation(ExcelListener.class).value();
                    if (excelListenerValues.length > 0) {
                        for (String excelListenerValue : excelListenerValues) {
                            if (StringUtils.isBlank(excelListenerValue)) {
                                continue;
                            }
                            map.put(excelListenerValue, analysisEventAutoListener);
                        }
                    } else {
                        String simpleName = c.getSimpleName();
                        String excelListenerValue = simpleName.replaceFirst(simpleName.substring(0, 1), simpleName.substring(0, 1).toLowerCase());
                        map.put(excelListenerValue, analysisEventAutoListener);
                    }
                } else {
                    log.error("自定义AnalysisEventAutoListener需要继承com.xinjump.easyexcel.event.AnalysisEventAutoListener");
                }
            }
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ExcelManager.applicationContext = applicationContext;
    }

}
