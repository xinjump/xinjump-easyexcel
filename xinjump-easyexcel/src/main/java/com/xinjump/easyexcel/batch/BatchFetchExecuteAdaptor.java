package com.xinjump.easyexcel.batch;

import java.util.List;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/10/27 16:06
 * @see com.xinjump.easyexcel.batch
 */
public class BatchFetchExecuteAdaptor<T, R> extends BaseBatchFetchExecutor<T, R> {

    @Override
    protected void doExecutePartWithoutReturn(List<T> orgiArgList, Object otherArg) {

    }

    @Override
    protected List<R> doExecutePage(Integer current, Integer size, Object otherArg) {
        return null;
    }

    @Override
    protected List<R> doExecutePart(List<T> orgiArgList, Object otherArg) {
        return null;
    }

    @Override
    protected List<R> doExecutePartBatch(List<List<T>> argList, Object otherArg) {
        return null;
    }

    @Override
    protected List<R> doExecuteFull(Object argList) {
        return null;
    }

}
