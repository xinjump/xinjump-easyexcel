package com.xinjump.easyexcel.batch;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author xinjump
 * @version 1.1
 * @date 2021/10/27 16:08
 * @see com.xinjump.easyexcel.batch
 */
@SuppressWarnings("unchecked")
public abstract class BaseBatchFetchExecutor<T, R> {

    private Integer BATCH_SIZE = 100;
    private List<R> fetchResultList = new ArrayList<>();
    private Map<String, Object> fetchResultMap = new HashMap<>();
    private Map<String, Set<Object>> fetchResultGroupMap = new HashMap<>();

    public void setBATCH_SIZE(Integer BATCH_SIZE) {
        this.BATCH_SIZE = BATCH_SIZE;
    }

    // ================================== doExecutePartWithoutReturn =====================================

    /**
     * 分批执行，不返回
     *
     * @param size       每次分批数
     * @param oriArgList 源实体
     * @param otherArg
     */
    public void partExecuteWithoutReturn(Integer size, List<T> oriArgList, Object otherArg) {
        initObject();
        size = defaultSize(size);
        if (oriArgList.size() <= size) {
            doExecutePartWithoutReturn(oriArgList, otherArg);
        } else {
            handleResultFun(oriArgList, null, null, size,
                    (partedArgList) -> {
                        doExecutePartWithoutReturn(partedArgList, otherArg);
                        return null;
                    }
            );
        }
    }


    // ================================== doExecutePage =====================================

    /**
     * 分批获取分页执行（无限循环直至没有查到数据）
     *
     * @param size     每次分批数
     * @param otherArg 条件
     * @return
     */
    public List<R> partFetchExecuteByPage(Integer size, Object otherArg) {
        initObject();
        size = defaultSize(size);
        int current = 1;
        for (; current < Integer.MAX_VALUE; current++) {
            List<R> singleList = doExecutePage(current, size, otherArg);
            if (singleList == null || singleList.isEmpty()) {
                break;
            }
            handlePartedResult(singleList, null, null);
        }
        return fetchResultList;
    }

    /**
     * 分批获取分页执行
     *
     * @param size     每次分批数
     * @param otherArg 条件
     * @param maxValue 最大查多少数据
     * @return
     */
    public List<R> partFetchExecuteByPage(Integer size, Object otherArg, Integer maxValue) {
        initObject();
        size = defaultSize(size);
        int current = 1, idxVal = (maxValue / size), newMaxVal = idxVal <= 0 ? 1 : idxVal;
        for (; current <= newMaxVal; current++) {
            List<R> singleList = doExecutePage(current, size, otherArg);
            if (singleList == null || singleList.isEmpty()) {
                break;
            }
            handlePartedResult(singleList, null, null);
        }
        return fetchResultList;
    }


    // ================================== doExecutePart =====================================

    /**
     * 分批按照条件查询转list
     *
     * @param oriArgList 条件1
     * @param otherArg   条件2...
     * @return
     */
    public List<R> partFetchListToList(List<T> oriArgList, Object otherArg) {
        initObject();
        if (oriArgList.size() <= BATCH_SIZE) {
            handlePartedResult(doExecutePart(oriArgList, otherArg), null, null);
        } else {
            handleResultFun(oriArgList, null, null, BATCH_SIZE, (partedArgList) -> doExecutePart(partedArgList, otherArg));
        }
        return fetchResultList;
    }

    /**
     * 分批按照条件查询转map（结果映射）
     *
     * @param oriArgList 条件1
     * @param otherArg   条件2...
     * @param keyDesc    设置根据哪些字段查询-key
     * @param valueKey   只查此key的值
     * @return
     */
    public Map<String, Object> partFetchListToMap(List<T> oriArgList, Object otherArg, String[] keyDesc, String valueKey) {
        initObject();
        if (oriArgList.size() <= BATCH_SIZE) {
            handlePartedResult(doExecutePart(oriArgList, otherArg), keyDesc, valueKey);
        } else {
            handleResultFun(oriArgList, keyDesc, valueKey, BATCH_SIZE, (partedArgList) -> doExecutePart(partedArgList, otherArg));
        }
        return fetchResultMap;
    }

    /**
     * 分批按照条件查询转map（结果映射）
     *
     * @param oriArgList 条件1
     * @param otherArg   条件2...
     * @param keyDesc    设置根据哪些字段查询-key
     * @param valueKey   只查此key的值
     * @return
     */
    public Map<String, Set<Object>> partFetchListToGroupMap(List<T> oriArgList, Object otherArg, String[] keyDesc, String valueKey) {
        initObject();
        if (oriArgList.size() <= BATCH_SIZE) {
            handlePartedResult(doExecutePart(oriArgList, otherArg), keyDesc, valueKey);
        } else {
            handleResultFun(oriArgList, keyDesc, valueKey, BATCH_SIZE, (partedArgList) -> doExecutePart(partedArgList, otherArg));
        }
        return fetchResultGroupMap;
    }


    // ================================== doExecutePartBatch =====================================

    /**
     * 批量按照条件查询转map（结果映射）
     *
     * @param oriArgList 条件1
     * @param otherArg   条件2
     * @param validDesc  过滤字段
     * @param keyDesc    设置根据哪些字段查询-key
     * @param valueKey   只查此key的值
     * @return
     */
    public Map<String, Object> partFetchListsToMap(List<List<T>> oriArgList,
                                                   Object otherArg,
                                                   String[] validDesc,
                                                   String[] keyDesc,
                                                   String valueKey) {
        if (oriArgList == null || oriArgList.isEmpty()) {
            return new HashMap<>();
        }
        initObject();
        List<Integer> argSizeList = new ArrayList<>();
        oriArgList.forEach(e -> argSizeList.add(e.size()));
        Integer size = argSizeList.get(0);

        argSizeList.forEach(e -> {
            assert (e.intValue() == size.intValue()) : "执行查询错误：参数列表数量不统一";
        });

        assert (oriArgList.size() == keyDesc.length) : "执行查询错误：参数列数量和参数描述数量不一致";

        if (oriArgList.size() <= BATCH_SIZE) {
            List<R> tmpList = doExecutePartBatch(oriArgList, otherArg);
            handlePartedResult(filterPairData(tmpList, oriArgList, validDesc), keyDesc, valueKey);
        } else {
            int currentMax = 0;
            for (int i = 0, partedCount = oriArgList.size() / BATCH_SIZE; i <= partedCount; i++) {
                currentMax += BATCH_SIZE;
                List<List<T>> partedArgList = new ArrayList<>();
                for (List<T> singleList : oriArgList) {
                    List<T> tmpSingle = singleList.subList((i * BATCH_SIZE), currentMax);
                    partedArgList.add(tmpSingle);
                }
                List<R> tmpList = doExecutePartBatch(partedArgList, otherArg);
                handlePartedResult(filterPairData(tmpList, oriArgList, validDesc), keyDesc, valueKey);
            }
            if (currentMax < oriArgList.size()) {
                List<List<T>> partedArgList = new ArrayList<>();
                for (List<T> singleList : oriArgList) {
                    List<T> tmpSingle = singleList.subList(currentMax, oriArgList.size());
                    partedArgList.add(tmpSingle);
                }
                List<R> tmpList = doExecutePartBatch(partedArgList, otherArg);
                handlePartedResult(filterPairData(tmpList, oriArgList, validDesc), keyDesc, valueKey);
            }
        }
        return fetchResultMap;
    }


    // ================================== doExecuteFull =====================================

    /**
     * 一次性查询之后进行结果映射
     *
     * @param argObj   条件
     * @param keyDesc  设置根据哪些字段查询-key
     * @param valueKey 只查此key的值
     * @return
     */
    public Map<String, Object> fullFetchObjectToMap(Object argObj, String[] keyDesc, String valueKey) {
        initObject();
        handlePartedResult(doExecuteFull(argObj), keyDesc, valueKey);
        return fetchResultMap;
    }

    /**
     * 一次性查询之后进行结果映射
     *
     * @param argObj   条件
     * @param keyDesc  设置根据哪些字段查询-key
     * @param valueKey 只查此key的值
     * @return
     */
    public Map<String, Set<Object>> fullFetchObjectToGroupMap(Object argObj, String[] keyDesc, String valueKey) {
        initObject();
        handlePartedResult(doExecuteFull(argObj), keyDesc, valueKey);
        return fetchResultGroupMap;
    }


    private void initObject() {
        if (!(fetchResultList == null || fetchResultList.isEmpty())) {
            fetchResultList.clear();
        }
        if (!(fetchResultMap == null || fetchResultMap.isEmpty())) {
            fetchResultMap.clear();
        }
        if (!(fetchResultGroupMap == null || fetchResultGroupMap.isEmpty())) {
            fetchResultGroupMap.clear();
        }
    }

    private Integer defaultSize(Integer size) {
        return (size == null || size < 0) ? BATCH_SIZE : size;
    }

    private List<R> filterPairData(List<R> dataList, List<List<T>> oriArgList, String[] validDesc) {
        if (dataList == null || dataList.isEmpty()) {
            return new ArrayList<>();
        }
        Object firstObj = dataList.stream().findFirst().orElse(null);
        List<Object> newDataList = new ArrayList<>();
        if (!(firstObj instanceof JSONObject)) {
            newDataList.addAll(JSON.parseArray(JSON.toJSONString(dataList), JSONObject.class));
        } else {
            newDataList.addAll(dataList);
        }
        List<R> resultList = new ArrayList<>();
        Set<String> recordSet = new HashSet<>();
        for (int i = 0, total = oriArgList.get(0).size(); i < total; i++) {
            StringBuilder singleBuilder = new StringBuilder();
            for (List<T> tList : oriArgList) {
                singleBuilder.append(tList.get(i)).append("-");
            }
            recordSet.add(singleBuilder.substring(0, singleBuilder.length() - 1));
        }

        for (Object obj : newDataList) {
            JSONObject jsonObject = (JSONObject) obj;
            StringJoiner contentJoiner = new StringJoiner("-");
            for (String singleVd : validDesc) {
                String tmpContent = jsonObject.getString(singleVd);
                if (tmpContent == null || tmpContent.length() == 0) {
                    tmpContent = "";
                }
                contentJoiner.add(tmpContent);
            }
            if (recordSet.contains(contentJoiner.toString())) {
                resultList.add((R) obj);
            }
        }
        return resultList;
    }

    /**
     * 通用循环调用执行函数
     *
     * @param oriArgList   源实体
     * @param keyDesc      设置根据哪些字段查询-key
     * @param valueKey     只查此key的值
     * @param size         每次处理条数
     * @param doExecuteFun 被执行的函数
     */
    private void handleResultFun(List<T> oriArgList, String[] keyDesc, String valueKey, Integer size, Function<List<T>, List<R>> doExecuteFun) {
        int currentMax = 0;
        for (int i = 0, partedCount = oriArgList.size() / size; i <= partedCount; i++) {
            currentMax += size;
            List<T> partedArgList = oriArgList.subList((i * size), currentMax);
            handlePartedResult(doExecuteFun.apply(partedArgList), keyDesc, valueKey);
        }
        // 处理剩余数据
        if (currentMax < oriArgList.size()) {
            List<T> partedArgList = oriArgList.subList(currentMax, oriArgList.size());
            handlePartedResult(doExecuteFun.apply(partedArgList), keyDesc, valueKey);
        }
    }

    /**
     * 通用处理结果（结果会进行去重）
     *
     * @param partResultList 结果集
     * @param keyDesc        设置根据哪些字段查询-key
     * @param valueKey       只查此key的值
     */
    private void handlePartedResult(List<R> partResultList, String[] keyDesc, String valueKey) {
        if (partResultList == null || partResultList.isEmpty()) {
            return;
        }
        if (keyDesc == null || keyDesc.length == 0) {
            fetchResultList.addAll(partResultList.stream().distinct().collect(Collectors.toList()));
            return;
        }
        List<Object> newPartResultList = new ArrayList<>();
        Object firstObj = partResultList.stream().findFirst().orElse(null);
        if (!(firstObj instanceof JSONObject)) {
            newPartResultList.addAll(JSON.parseArray(JSON.toJSONString(partResultList), JSONObject.class));
        } else {
            newPartResultList.addAll(partResultList);
        }
        Function<Object, String> keyFun = (e) -> {
            if (keyDesc.length == 1) {
                return ((JSONObject) e).getString(keyDesc[0]);
            } else {
                StringJoiner keyJoiner = new StringJoiner("-");
                for (String key : keyDesc) {
                    keyJoiner.add(((JSONObject) e).getString(key));
                }
                return keyJoiner.toString();
            }
        };
        Function<Object, Object> valueFun = (e) -> {
            if (valueKey == null || valueKey.length() == 0) {
                return e;
            } else {
                String val = ((JSONObject) e).getString(valueKey);
                return (val == null || val.length() == 0) ? "" : val;
            }
        };
        newPartResultList.forEach(e -> {
            String key = keyFun.apply(e);
            Object val = valueFun.apply(e);
            fetchResultMap.put(key, val);

            if (fetchResultGroupMap.containsKey(key)) {
                fetchResultGroupMap.get(key).add(val);
            } else {
                Set<Object> set = new HashSet<>();
                set.add(val);
                fetchResultGroupMap.put(key, set);
            }
        });
    }


    // 批处理，执行部分不返回
    protected abstract void doExecutePartWithoutReturn(List<T> oriArgList, Object otherArg);

    // 执行分页
    protected abstract List<R> doExecutePage(Integer current, Integer size, Object otherArg);

    // 执行部分
    protected abstract List<R> doExecutePart(List<T> oriArgList, Object otherArg);

    // 必须进行结果映射
    // 执行部分批量
    protected abstract List<R> doExecutePartBatch(List<List<T>> oriArgList, Object otherArg);

    protected abstract List<R> doExecuteFull(Object oriArgList);

}
