package com.xinjump.easyexcel.annotation;

import java.lang.annotation.*;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/13 15:49
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Excel {

    String fileName() default "";

    String sheetName() default "";

}
