package com.xinjump.easyexcel.annotation;

import java.lang.annotation.*;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/14 17:47
 * @see com.xinjump.easyexcel.annotation
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelListener {

    String[] value() default {};

}
