package com.xinjump.easyexcel.annotation;

import com.xinjump.easyexcel.enums.BaseEnum;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ExcelValidated.Validator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelValidated {

    String message() default "";

    String sample() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Class<? extends BaseEnum> enumClass();

    boolean isKeyValid() default true;

    class Validator implements ConstraintValidator<ExcelValidated, Object> {

        private ExcelValidated annotation;

        @Override
        public void initialize(ExcelValidated validated) {
            annotation = validated;
        }

        @Override
        public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {
            if (value == null) {
                return true;
            }

            try {
                if (annotation.isKeyValid()) {
                    BaseEnum.keyOf(annotation.enumClass(), value);
                } else {
                    BaseEnum.valueOf(annotation.enumClass(), value);
                }
            } catch (IllegalStateException e) {
                return false;
            }
            return true;
        }
    }

}
