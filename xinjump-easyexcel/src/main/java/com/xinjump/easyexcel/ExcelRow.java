package com.xinjump.easyexcel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.xinjump.easyexcel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author xinjump
 * @version 1.1
 * @date 2020/12/16 15:38
 * @see com.xinjump.easyexcel
 */
@Excel(fileName = "导入时错误信息", sheetName = "错误信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ColumnWidth(value = 25)
@HeadRowHeight(value = 30)
@ContentRowHeight(value = 20)
public class ExcelRow implements Serializable {

    @ExcelIgnore
    private int rowNum;

    @ExcelProperty(value = "错误信息")
    private String validateMessage;

    public void setValidateMessage(String validateMessage) {
        this.validateMessage = StringUtils.isBlank(this.validateMessage) ? validateMessage
                : this.validateMessage + "\n" + validateMessage;
    }

}
